# Itemis coding challange - Sales Tax calculator

This project is a Sales Tax Calculator application that calculates taxes for purchased items and generates a receipt.

## Installation

1. Clone the repository:
   ```shell
   https://gitlab.com/dzafe1987/itemis_coding_challange.git

2. Navigate to the project directory:
    ```shell
    cd itemis_coding_challange  
3. Install the dependencies:
    ```shell
   npm install

## Installation

1. Update the inputs in the app.ts file with your desired items for calculation.

2. Run the application:
    ```shell
    npm run

&bull; This will calculate the taxes and generate a receipt based on the provided inputs.

3. To run the tests, use the following command:
    ```shell
    npm test

4. To fix the code style issues, use the following:
    ```shell
    npm run lint

