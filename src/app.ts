import ReceiptService from './services/ReceiptService';
import Item from './models/Item';

const receiptService: ReceiptService = new ReceiptService();

// Input 1
const item1: Item = {
  name: 'book',
  price: 12.49,
  quantity: 1,
  imported: false,
  exempted: true
};

const item2: Item = {
  name: 'music CD',
  price: 14.99,
  quantity: 1,
  imported: false,
  exempted: false
};

const item3: Item = {
  name: 'chocolate bar',
  price: 0.85,
  quantity: 1,
  imported: false,
  exempted: true
};

receiptService.addItem(item1);
receiptService.addItem(item2);
receiptService.addItem(item3);

console.log('Output 1:');
receiptService.printReceipt();
console.log();

// Input 2
const item4: Item = {
  name: 'imported box of chocolates',
  price: 10.00,
  quantity: 1,
  imported: true,
  exempted: true
};

const item5: Item = {
  name: 'imported bottle of perfume',
  price: 47.50,
  quantity: 1,
  imported: true,
  exempted: false
};

receiptService.addItem(item4);
receiptService.addItem(item5);

console.log('Output 2:');
receiptService.printReceipt();
console.log();

// Input 3
const item6: Item = {
  name: 'imported bottle of perfume',
  price: 27.99,
  quantity: 1,
  imported: true,
  exempted: false
};

const item7: Item = {
  name: 'bottle of perfume',
  price: 18.99,
  quantity: 1,
  imported: false,
  exempted: false
};

const item8: Item = {
  name: 'packet of headache pills',
  price: 9.75,
  quantity: 1,
  imported: false,
  exempted: true
};

const item9: Item = {
  name: 'box of imported chocolates',
  price: 11.25,
  quantity: 1,
  imported: true,
  exempted: true
};

receiptService.addItem(item6);
receiptService.addItem(item7);
receiptService.addItem(item8);
receiptService.addItem(item9);

console.log('Output 3:');
receiptService.printReceipt();
