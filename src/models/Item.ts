interface Item {
    name: string;
    price: number;
    quantity: number;
    imported: boolean;
    exempted: boolean;
}

export default Item;
