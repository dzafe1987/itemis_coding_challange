import Item from '../models/Item';

class ReceiptService {
  private items: Item[] = [];

  /**
     * Adds an item to the receipt.
     * @param item The item to be added.
     */
  public addItem(item: Item): void {
    this.items.push(item);
  }

  /**
     * Calculates the tax amount for an item.
     * @param item The item for which to calculate the tax.
     * @returns The tax amount for the item.
     */
  public calculateTax(item: Item): number {
    let taxAmount = 0;

    // Calculate tax based on exemption and import status
    if (!item.exempted) {
      taxAmount += item.price * 0.1;
    }

    if (item.imported) {
      taxAmount += item.price * 0.05;
    }

    // Round up to the nearest 0.05
    taxAmount = Math.ceil(taxAmount * 20) / 20;

    return taxAmount;
  }

  /**
     * Prints the receipt details including items, taxes, and total cost.
     * *Note that method will reset items after each print of receipt*
     */
  public printReceipt(): void {
    let total = 0;
    let totalTax = 0;

    for (const item of this.items) {
      const taxAmount = this.calculateTax(item);
      const itemTotal = item.price + taxAmount;

      console.log(`${item.quantity} ${item.name}: ${itemTotal.toFixed(2)}`);

      total += itemTotal;
      totalTax += taxAmount;
    }

    console.log(`Sales Taxes: ${totalTax.toFixed(2)}`);
    console.log(`Total: ${total.toFixed(2)}`);
    // Reset the items to the initial value
    this.items = [];
  }
}

export default ReceiptService;
