import ReceiptService from '../src/services/ReceiptService';
import Item from '../src/models/Item';
import { describe, it, expect, beforeEach } from '@jest/globals';


describe('ReceiptService', () => {
  let receiptService: ReceiptService;

  beforeEach(() => {
    receiptService = new ReceiptService();
  });

  it('should calculate the tax amount correctly for exempted items', () => {
    // Test case for exempted item
    const item: Item = {
      name: 'book',
      price: 12.49,
      quantity: 1,
      imported: false,
      exempted: true
    };

    const taxAmount = receiptService.calculateTax(item);

    expect(taxAmount).toBe(0); // No tax should be applied to exempted items
  });

  it('should calculate the tax amount correctly for non-exempted and imported items', () => {
    // Test case for non-exempted and imported item
    const item: Item = {
      name: 'imported bottle of perfume',
      price: 27.99,
      quantity: 1,
      imported: true,
      exempted: false
    };

    const taxAmount = receiptService.calculateTax(item);

    expect(taxAmount).toBeCloseTo(4.2, 2);
  });
});
